workflow bbtools {
    File reads
    File ref

    call alignment {
       input: fastq=reads,
              fasta=ref
    }
    call samtools {
       input: sam=alignment.sam
   }
}

task alignment {
    File fastq
    File fasta

    command {
        shifter --image=jfroula/aligner-bbmap:1.1.3 bbmap.sh in=${fastq} ref=${fasta} out=test.sam
    }

    output {
       File sam = "test.sam"
    }
}

task samtools {
    File sam

    command {
       shifter --image=jfroula/aligner-bbmap:1.1.3 samtools view -b -F0x4 ${sam} | samtools sort - > test.sorted.bam
    }

    output {
       File bam = "test.sorted.bam"
    }
}
