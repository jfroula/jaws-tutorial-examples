# Summary
These are some helloworld example WDLs to demonstrate different functionality, like how to run tasks in parallel or how to access a reference database, etc.  

# Running the example
Assuming you have successfully set up globus and the other configuration requirements outlined 
in [Quickstart Example](https://jaws-docs.readthedocs.io/en/latest/Tutorials/jaws_quickstart.html) in the JAWS docs, do the following.

```
ssh cori.nersc.gov
source ~/jaws-dev/bin/activate
jaws
```

