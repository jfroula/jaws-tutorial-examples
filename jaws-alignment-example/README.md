## Summary
This is an example running a pipeline in JAWS. 
It demonstrates the usage of a docker image, a subworkflow and how sharding of an input file is done.

# run jaws submit <workflow> <inputs> <subworkflow>
```
jaws run submit main.wdl inputs.json out nersc
```
