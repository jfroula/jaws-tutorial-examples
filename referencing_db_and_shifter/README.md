# How to Access Reference Databases
in your commands section in the WDL, you use `/refdata` as a root, and then add whatever specific database directory you want, for example to use  the blast nt database from NCBI you would have a command
`blastn -db /refdata/nt/nt`
where the first `nt` is the directory with all the index files and the second `nt` is the prefix to the index files (i.e. `nt.nih`).

# Run Example 
This workflow just lists the contents of the nt database.

# run with jaws
This is an example running a pipeline in JAWS. 
It demonstrates the usage of a docker image and using a reference db.

# run jaws submit <workflow> <inputs> <subworkflow>
```
jaws run submit alignment.wdl inputs.align.json out nersc
```

