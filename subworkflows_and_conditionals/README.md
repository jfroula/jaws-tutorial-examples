# Running sub-workflows and using conditional statements
This example includes two wdl sub-workflows, and which one is run depends on some flag (using conditional statements).

### run it in jaws
```
jaws run submit main.wdl inputs.json out nersc
```  

